#' @import dplyr plyr bnutil pgUpstream ggplot2 shinyjs
#' @export
shinyServerRun = function(input, output, session, context) {
  getFolderReactive = context$getRunFolder()
  getDataReactive = context$getData()
  output$body = renderUI({
        sidebarLayout(
                      sidebarPanel(
                          tags$div(HTML("<strong><font color = green>Upstream Kinase Analysis 86402</font></strong>")),
                          tags$hr(),
                          actionButton("start", "Start"),
                          tags$hr(),
                          textOutput("status")

                      ),
                      mainPanel(shinyjs::useShinyjs(),
                        tabsetPanel(
                          tabPanel("Basic Settings",
                                   tags$hr(),
                                   sliderInput("scan", "Scan Rank From-To", min = 1, max = 20, value = c(4,12),round = TRUE),
                                   sliderInput("nperms", "Number of permutations", min = 100, max = 1000, value = 500, step = 100, round = TRUE)
                                 ),

                          tabPanel("Advanced Settings"                                   ,
                                   tags$hr(),
                                   helpText("Set weights for database types:"),
                                   sliderInput("wIviv", "In Vitro / In Vivo", min = 0, max = 1, value = 1),
                                   sliderInput("wPhosphoNET", "In Silico (PhosphoNET)", min = 0, max = 1, value = 1),
                                   tags$hr(),
                                   helpText("Set minimal sequence homology required to link a peptide to a phosphosite"),
                                   sliderInput("seqHom", "Minimal sequence homology", min =0, max = 1, value =0.9),
                                   tags$hr(),
                                   numericInput("minPScore", "Minimal PhosphoNET prediction score", value = 300),
                                   tags$hr(),
                                   checkboxInput("seed", "Set seed for random permutations"))
                        )
                      )
        )
  })

  getGroupingLabel = function(aData){
    if (length(aData$colorLabels) > 1){
      stop("Need at most one Color to define the grouping")
    }
    else if(length(aData$colorLabels) == 1){
      groupingLabel = aData$colorColumnNames
      grp = as.factor((aData$data[[groupingLabel]]))
      if (length( levels(grp) ) != 2){
        stop(paste("Wrong number of groups, found:", levels(grp)))
      }

      df = subset(aData$data, rowSeq = 1)
      grp = as.factor((df[[groupingLabel]]))
      if (min(summary(grp)) < 2){
        stop("Need at least 2 observations per group.")
      }
      return(groupingLabel)
    }
    else {
      if(max(aData$data$colSeq)> 1){
        stop(paste("Can not run this data without a grouping factor.", aData$colorLabels))
      } else return(NULL)
    }
  }

  nid = showNotification("Press Start to start the analysis.", duration = NULL, type = "message", closeButton = FALSE)
  updateSliderInput(session, "seqHom", min = min(UpstreamApp86402::UpstreamDatabase$PepProtein_SeqHomology))
  
  observe({
    getData=getDataReactive$value
    getFolder = getFolderReactive$value
    if (is.null(getData)) return()
    adf = getData()
    output$status = renderText({
      grp = getGroupingLabel(adf)
      if (input$start == 0){
        if (!is.null(grp)){
          return(paste("Grouping factor:", grp))
        } else {
          return("Grouping factor: none")
        }
      }
      
      shinyjs::disable("scan")
      shinyjs::disable("nperms")
      shinyjs::disable("wIviv")
      shinyjs::disable("wPhosphoNET")
      shinyjs::disable("seqHom")
      shinyjs::disable("minPScore")
      
      if(!adf$hasUniqueDataMapping) stop("Mapping error: not all input data is unique")
      
      if(adf$hasMissingCells) stop("Missing values are not allowed.")
      
      if(adf$getMaxNPerCell() > 1) stop("More than one value per cell in the cross-tab view is not allowed.")
         
      df = adf$getData()
      
      db = UpstreamApp86402::UpstreamDatabase
      db = db %>% filter(PepProtein_SeqHomology >= input$seqHom) 
      db = db %>% filter(Kinase_PKinase_PredictorVersion2Score >= input$minPScore | Database != "phosphoNET")
      
      nCores = detectCores()
      msg = paste("Please wait ... running analysis. Using", nCores, "cores.")
      showNotification(ui = msg, id = nid, type = "message", closeButton = FALSE, duration = NULL)
      
      if(input$seed){
        set.seed(42)
      }
      
      if(!is.null(grp)){
        df$grp = df[[grp]]
        aResult = pgScanAnalysis2g(df,  dbFrame = db,
                                  scanRank = input$scan[1]:input$scan[2],
                                  nPermutations = input$nperms,
                                  dbWeights = c(iviv = input$wIviv,
                                                PhosphoNET = input$wPhosphoNET
                                                ))
      } else {
        aResult = pgScanAnalysis0(df,  dbFrame = db,
                                 scanRank = input$scan[1]:input$scan[2],
                                 nPermutations = input$nperms,
                                 dbWeights = c(iviv = input$wIviv,
                                               PhosphoNET = input$wPhosphoNET
                                 ))
      }
      showNotification(ui = "Done", id = nid, type = "message", closeButton = FALSE)
      aFull = ldply(aResult, .fun = function(.)return(data.frame(.$aResult, mxRank = .$mxRank) ))


      settings = data.frame(setting = c("ScanRank Min", "ScanRank Max", "Number of Permutations", "In Vitro In Vitro weight", "PhosphoNET weight", "Min PhosphoNet score", "Min Sequence Homology"),
                            value   = c(input$scan[1] , input$scan[2], input$nperms, input$wIviv, input$wPhosphoNET, input$minPScore, input$seqHom) )

      spath = file.path(getFolder(), "runData.RData")
      save(file = spath, df, aResult, aFull, settings)
      out = data.frame(rowSeq = 1, colSeq = 1, dummy = NaN)
      meta = data.frame(labelDescription = c("rowSeq", "colSeq", "dummy"), groupingType = c("rowSeq", "colSeq", "QuantitationType"))
      result = AnnotatedData$new(data = out, metadata = meta)
      context$setResult(result)
      return("Done")
    })

  })
}

#' @export
#' @import colourpicker kinaseTreeParser
shinyServerShowResults = function(input, output, session, context){
  getFolderReactive = context$getRunFolder()
  output$body = renderUI(
    sidebarLayout(
      sidebarPanel(
        tags$div(HTML("<strong><font color = green>Upstream Kinase Analysis 86402</font></strong>")),
        tags$hr(),
        textOutput("grpText"),
        tags$hr(),
        sliderInput("minsetsize", "Include results based on peptide sets with minimal size of:",
                    min = 1,
                    max = 5,
                    step = 1,
                    value = 3),
        width = 3),
      mainPanel(
        tabsetPanel(
          tabPanel("Upstream Kinase Score",
                   helpText("This plot shows putative upstream kinases ranked by their Final Score (median) or the value of the Kinase Statistic (median)."),
                   helpText("Each point is the result of an individual analysis with a different rank cut-off for adding upstream kinases for peptides."),
                   helpText("The size of the points indicates the size of the peptide set used for a kinase in the corresponding analysis."),
                   helpText("The color of the points indicates the specificity score resulting from the corresponding analysis."),
                   selectInput("spsort", "Sort score plot on", choices = list(Score = "score", Statistic = "stat")),
                   actionLink("saveScorePlot", "Save score plot"),
                   plotOutput("scorePlot", height = "1400px")
          ),
          tabPanel("Kinase Volcano",
                   helpText("This plot shows the median Final Score (y-axis) versus the mean Kinase Statistic (x-axis) of putative upstream kinases."),
                   helpText("The size of the kinase names indicates the size of the peptide set used for a kinase in the corresponding analysis."),
                   helpText("The color of the kinase names indicates the specificity score resulting from the corresponding analysis."),
                   plotOutput("volcanoPlot", height = "800px")
          ),
          tabPanel("Kinase Details",
                   helpText("These are kinase details"),
                   selectInput("showKinase", "Select kinase", choices = ""),
                   tags$hr(),
                   actionLink("uniprot", "..."),
                   tags$hr(),
                   tableOutput("kinaseSummary"),
                   tabsetPanel(
                     tabPanel("Details Table",
                              helpText(""),
                              actionLink("saveDetailsTable", "Save details table"),
                              tableOutput("kinaseDetails")
                     ),
                     tabPanel("Per peptide plot",
                              helpText(""),
                              actionLink("savePerpeptidePlot", "Save per peptide plot"),
                              plotOutput("perPeptidePlot", height = "800px")

                     )
                   )
          ),
          tabPanel("Report",
                   helpText("The table below shows the settings that were used for this analysis."),
                   tableOutput("InfoSettings"),
                   helpText("The below shows the summary results of the analysis"),
                   actionLink("saveSummaryResults", "Save summary results"),
                   tableOutput("SummaryTable")
          ),
          tabPanel("Kinase tree",
                   tags$a(href = "http://kinhub.org/kinmap/","The summary data can be saved as a file that can be used to map the data to a phylogenetic tree using the external websit Kinmap."),
                   helpText(""),
                   actionButton("saveKinMap", "Save data as KinMap file"),
                   tags$hr(),
                   helpText("The specificty score will be mapped to the symbol size"),
                   textInput("sclow", "Scale score from:", 0),
                   textInput("schigh","Scale score to:", 2),
                   textInput("scmax", "max symbol size", 50),
                   tags$hr(),
                   helpText("The kinase statistic will be mapped to the symbol color"),
                   textInput("stlow", "Scale kinase statistic from:", -1),
                   textInput("stmid", "Scale kinase statistic midpoint:", 0),
                   textInput("sthigh","Scale kinase statistic to:", 1),
                   colourpicker::colourInput("cllow", label = "Low color", value = "green"),
                   colourpicker::colourInput("clmid", label = "Mid color", value = "black"),
                   colourpicker::colourInput("clhigh", label = "High color", value = "red")
          )
        )
      )

    )
  )

  observe({
    getFolder = getFolderReactive$value
    if(is.null(getFolder)) return()
    spath = file.path(getFolder(), "runData.RData")
    load(file = spath)
    minSeqHom = settings$value[settings$setting == "Min Sequence Homology"]
    minPScore = settings$value[settings$setting ==  "Min PhosphoNet score"]
    
    db = UpstreamApp86402::UpstreamDatabase
    db = db %>% filter(PepProtein_SeqHomology >= minSeqHom) 
    db = db %>% filter(Kinase_PKinase_PredictorVersion2Score >= minPScore | Database != "phosphoNET")
    
    kinase2uniprot = db%>%group_by(Kinase_UniprotID)%>% dplyr::summarise(Kinase_Name = Kinase_Name[1])
    

    output$grpText = renderText({
        grp = df[["grp"]]
        if (!is.null(grp)){
          txt = paste("Grouping factor with levels", levels(grp)[1], "and", levels(grp)[2])
        } else {
          return("Grouping factor: none")
        }
    })

    xaxText = function(){
      grp = df[["grp"]]
      if (!is.null(grp)){
        txt = paste(">0 indicates higher activity in the",levels(grp)[2], "group")
      } else {
        return(NULL)
      }
    }

    scorePlot = reactive({
      aSub = subset(aFull, nFeatures >= input$minsetsize)
      cs = makeScorePlot(aSub, input$spsort)
      xax = paste("Normalized kinase statistic (", xaxText(),")", sep = "")
      cs = cs + ylab(xax)
    })
    
    perPeptidePlot = reactive({
      
      aPlot = makePerPeptidePlot(df,
                                 subset(db , Kinase_Name == input$showKinase),
                                 scanRank = subset(settings, setting == "ScanRank Max")$value,
                                 minPScore = subset(settings,setting == "Min PhosphoNet score")$value)
      return(aPlot)
    })

    output$scorePlot = renderPlot({
      print(scorePlot())
    })

    aSummary = reactive({
      aSub = subset(aFull, nFeatures >= input$minsetsize)
      aSum = makeSummary(aSub)
      updateSelectInput(session, "showKinase", choices = aSum$ClassName)
      aSum
    })

    output$volcanoPlot = renderPlot({
      vp = makeVolcanoPlot(aSummary())
      print(vp)
    })

    output$perPeptidePlot = renderPlot({
      print(perPeptidePlot())
    })

    output$kinaseSummary = renderTable({
      aSum = aSummary()
      aKin = subset(aSum, ClassName == input$showKinase)
      aTable = data.frame(name  = c("Mean Kinase Statistic", "Mean Specificity Score", "Mean Significance Score", "Median Combined Score"),
                          value = c(aKin$meanStat, aKin$meanFeatScore, aKin$meanPhenoScore, aKin$medianScore))
      colnames(aTable) = c(input$showKinase, "value")
      return(aTable)
    })

    observeEvent(input$showKinase, {
      upid = kinase2uniprot %>% filter(Kinase_Name == input$showKinase)%>%.$Kinase_UniprotID
      aLabel = paste(input$showKinase," (",upid,") on Uniprot.org", sep = "")
      updateActionButton(session, "uniprot", label = aLabel)
    })


    detailsTable = reactive({
      aTable = makeDetailsTable(df,
                                subset(db , Kinase_Name == input$showKinase),
                                scanRank = subset(settings, setting == "ScanRank Max")$value,
                                minPScore = subset(settings,setting == "Min PhosphoNet score")$value)
    })

    output$kinaseDetails = renderTable({
        aTable = detailsTable()
    })

    output$InfoSettings = renderTable({
      getSettingsInfo(settings)
    })

    observeEvent(input$saveDetailsTable, {
      aTable = detailsTable()
      filename = file.path(getFolder(), paste(gsub("/", "-",input$showKinase), format(Sys.time(), "%Y%m%d-%H%M.txt")) )
      write.table(aTable, filename, sep = "\t", quote = FALSE, row.names = FALSE)
      shell.exec(getFolder())
    })

    observeEvent(input$saveScorePlot, {
      filename = file.path(getFolder(), paste("UpstreamScorePlot", format(Sys.time(), "%Y%m%d-%H%M.png")) )
      ggsave(filename, scorePlot(), device = "png" ,units = "cm", height = 40, width = 28)
      shell.exec(getFolder())
    })
    
    observeEvent(input$savePerpeptidePlot, {
      filename = file.path(getFolder(), paste(gsub("/", "-",input$showKinase),"_peptides", format(Sys.time(), "%Y%m%d-%H%M.png")) )
      ggsave(filename, perPeptidePlot(), device = "png" ,units = "cm", height = 40, width = 28)
      shell.exec(getFolder())
    })

    observeEvent(input$uniprot, {
      upid = kinase2uniprot %>% filter(Kinase_Name == input$showKinase)%>%.$Kinase_UniprotID
      browseURL(paste("http://www.uniprot.org/uniprot/", upid, sep = ""))
    })

    summaryResultTable = reactive({
      df = aSummary()
      df = left_join(kinase2uniprot, df, by = c("Kinase_Name" = "ClassName"))
      df = df%>% filter(!is.na(medianScore))%>%arrange(-medianScore)
    })

    observeEvent(input$saveSummaryResults, {
      aTable = summaryResultTable()
      filename = file.path(getFolder(), paste("Summaryresults", format(Sys.time(), "%Y%m%d-%H%M.txt")) )
      write.table(aTable, filename, sep = "\t", quote = FALSE, row.names = FALSE)
      shell.exec(getFolder())
    })
    
    output$SummaryTable = renderTable({
      summaryResultTable()
    })
    
    observeEvent(input$saveKinMap, {
      df = summaryResultTable()
      filename = file.path(getFolder(), paste("KinMap file", format(Sys.time(), "%Y%m%d-%H%M.txt")) )
      szFrom = c(as.numeric(input$sclow), as.numeric(input$schigh))
      szTo = c(0, as.numeric(input$scmax))
      szScale = list(from = szFrom, to = szTo)
      clScale = list(low = as.numeric(input$stlow), mid = as.numeric(input$stmid), high = as.numeric(input$sthigh))
      clr = c(input$cllow, input$clmid, input$clhigh)
      treeFile(fname = filename, mappings = Kinase_Name ~ meanStat + meanFeatScore, data = df,szScale = szScale, clScale = clScale, clValues = clr)
      shell.exec(getFolder())
    })
  })
}
